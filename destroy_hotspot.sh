#!/bin/bash

# Stop services
systemctl stop dnsmasq
systemctl stop hostapd

# Restore config files
mv /etc/dhcpcd.conf /etc/dhcpcd.pi2
mv /etc/dhcpcd.bkp /etc/dhcpcd.conf

service dhcpcd restart

mv /etc/dnsmasq.conf /etc/dnsmasq.pi2
mv /etc/dnsmasq.bkp /etc/dnsmasq.conf

mv /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.pi2
mv /etc/default/hostapd /etc/default/hostapd.pi2
mv /etc/default/hostapd.bkp /etc/default/hostapd

# Restart all services
service networking restart
systemctl start hostapd
systemctl start dnsmasq
