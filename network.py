from subprocess import call
import time


wpa = open('/etc/wpa_supplicant/wpa_supplicant.conf', 'ab+')
wpa_str = wpa.read()

def include_password(ssid, password):
    wpa.write('\nnetwork={\n')
    wpa.write('     ssid="%s"\n' % ssid)
    wpa.write('     psk="%s"\n' % password)
    wpa.write('     key_mgmt=WPA-PSK\n')
    wpa.write('}\n')


if 'network' not in wpa_str:
    print('Initializing hotspot...')
    call(['./create_hotspot.sh'])
    time.sleep(40)
    print('Enter SSID: ')
    net_name = raw_input()
    print('Enter password: ')
    net_pass = raw_input()
    include_password(net_name, net_pass)
    print("Tearing down hotspot")
    call(['./destroy_hotspot.sh'])

