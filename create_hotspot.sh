#!/bin/bash

# Stop services
systemctl stop dnsmasq
systemctl stop hostapd

# Replace config files
mv /etc/dhcpcd.conf /etc/dhcpcd.bkp
mv /etc/dhcpcd.pi2 /etc/dhcpcd.conf

service dhcpcd restart

mv /etc/dnsmasq.conf /etc/dnsmasq.bkp
mv /etc/dnsmasq.pi2 /etc/dnsmasq.conf

mv /etc/hostapd/hostapd.pi2 /etc/hostapd/hostapd.conf
mv /etc/default/hostapd /etc/default/hostapd.bkp
mv /etc/default/hostapd.pi2 /etc/default/hostapd

# Restart all services
service networking restart
systemctl start hostapd
systemctl start dnsmasq

